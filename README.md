## Objective:
Goal is to make web applications from easily hackable to difficult. This is one of several machines that will eventually adjust and fix bugs intended to be exploited at first. The previous [machine](https://gitlab.com/archerl/login_form_poor_cookies) dealt with Business Login Error and a weak cryptographic problem (not because any of it was used weakly there, but because of its lack of usage to make the cookies more discernible). 

The user and password are protected, for example, in this machine. But again the idea is to make the user think of the other most obvious way to impersonate the authenticated user, i.e., via cookies. This is what the challenge is, make the user think in the direction of how web application works. 

In the real world situation, this would be considered as information disclosure and cryptographic issue, because these two factors combined together help capture the flag.

## Implementation
The app would/was built on basic nodemodules and express-js. To aide the simplicity anobject-database was used (static JS database file) was used. The initial sourced code can be found [here](https://gitlab.com/archerl/login_form_poor_cookies_2) and a live demo instance of the website [here](https://login-form-poor-cookies-2.herokuapp.com/).

A dockerfile was also made to aide the deployment. Run the dockerfile with commands

1. `docker build -t <your username>/progressive-ctf-1 .`

2. `docker run -p anyport_out:3000 -d <your username>/progressive-ctf-1`


To check if it ran successfully or not.

`docker ps`

Open your browser and go to the `localhost:anyport_out`, to be able to run it with burp use `localhost.:anyport_out`

## features required
Before deploying, it is recommended to have a rate-throttling or rate-limiting in place on the endpoint.

## Constraints
The machine/web-app is very easy and its better to used under a time boundation for any CTF's purposes.

## Problem Statement

Problem: The website was built by Military police, like we all know they are lazy and incompetent, the website they built is very new and 
lacks security. Since you are Levi, you have to figure out a way to hack into Erwin Smith's account yet again. Eren has joined to help you
as well.

Your credentials are: 
username: "levi"
password: "password"

Eren's credentials are:
username: "eren"
password: "passwor%^&&42d4"

Login in to the account, read the message from the section commander Hanje, understand the working of the website, how the authentication 
is being done & finish the CTF.

Hints:
1. You don't only need the password and username to make the system believe you are the owner of the account.
2. There are ways to authenticate yourself, how do you think the session is being validated? how is different from the previous one?
3. Look at the source code of the login page, enumerate harder.
4. All JS files are important!
5. Can you generate the token?

TRIVIA: Based on the Characters of the Manage and anime by the same name, Attack on titan. The commander Erwin Smith has been arrested by
millitary police. He was to be sent a secret code/flag in his account's inbox. After his arrest his account was suspended and password 
changed, but the code is very vital for the survey core and must be retieved at any cost. He now tasks Captain Levi to hack into his 
account on the website.


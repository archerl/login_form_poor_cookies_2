const secretKey = 'IamSecretKey';

const cipher = salt => {
    const textToChars = text => text.split('').map(c => c.charCodeAt(0));
    const byteHex = n => ("0" + Number(n).toString(16)).substr(-2);
    const applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code);

    return text => text.split('')
        .map(textToChars)
        .map(applySaltToChar)
        .map(byteHex)
        .join('');
}

// THIS IS A HINT, since this points to the this JS File itself, can be enabled to make the CTF easy!
console.log('You are clever, but try harder!')

function setToken(){
    var username = document.getElementById('userName').value;
    var generrateSessionToken = cipher(secretKey);
    var token = generrateSessionToken(username);
    document.getElementById("set-token").value = token;
}
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const hbs = require('hbs');

const USERS = require('../database/users');

// Take any port
const port = process.env.PORT || 3000;


const app = express();
const publicDirectortPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');


app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);


app.use(express.static(publicDirectortPath));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


app.get('/', (req, res) => {

    const username = req.cookies.username;
    const token = req.cookies.token;

    if (username && token && USERS[username]) {
        if(USERS[username].token === token){
            res.render('message', {
                username: username,
                message: USERS[username].message
            })
        }else{
            res.send(`Invalid token for ${username}, click <a href=\'/\'>here</a> to login again`);
        }
    } else {
        res.render('index')
    }
});

app.post('/login', (req, res) => {
    const username = req.body.username.toLowerCase();

    if (USERS[username] && req.body.password === USERS[username].password) {
        res.cookie('token', req.body.token);
        res.cookie('username', req.body.username);
        res.redirect('/')
    } else {
        res.send('Invalid user or pass, click <a href=\'/\'>here</a> to go back');
    }

})

app.get('/logout', (req, res) => {
    res.clearCookie('token');
    res.clearCookie('username');
    res.redirect('/')
})

app.listen(port, () => {
    console.log(`Server running at port ${port}`)
})
const USERS = {
    levi: {
        password: 'password',
        token: '4841524d',
        message: 'This is Hanje, good job Levi, you were able to retrieve the code from Erwin\'s account, even though the Military Police were able to detect a leak and have now upgraded their website. Unfortunately, another of the same codes will be sent back to Erwin\'s account, and you will have to hack the website again and steal the code for us. As always, we count on you to keep the future of the SurveyCore alive.'
    },
    erwin: {
        password: '^7bkuasdkhg^%73',
        token: '4156534d4a',
        message: 'Congratulations, you have found the flag{_wof2_}'
    },
    sasha: {
        password: 'pa^7bkuasdkhg^%73',
        token: '5745574c45',
        message: 'Please don\'t eat/steal food without the permission of canteen authorities!',
    },
    mikasa: {
        password: 'pas^7bkuasdkhg^%73sword3',
        token: '494d4f455745',
        message: 'You can\'t beat Captain Levi!.'
    },
    eren: {
        password: 'passwor%^&&42d4',
        token: '4156414a',
        message: 'Are you a titan or a human? or are you a potato??'
    },
    riener: {
        password: 'passwo&^&^%W&rd5',
        token: '564d414a4156',
        message: 'Marko!!!!!!!!!!!!!!!!!!!!'
    }
};


module.exports = USERS;